import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { StoreRouterConnectingModule, DefaultRouterStateSerializer } from '@ngrx/router-store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { ChallengeLoadEffects } from './application/effects/challenges/loadChallengeEffect';
import { reducer } from './application/reducers/global';
import { InitializerEffect } from '@effects/global/initializeEffect';
import { SharedModule } from './framework/shared/shared.module';
import { SaveStateEffect } from '@effects/global/saveStateEffect';
import { FirstTimeEffect } from '@effects/global/firstTimeEffect';
import { ChallengePouchService } from './infrastructure/challenge-pouch.service';
import { ChallengeService } from './application/services/challenge.service';
import { PersistentStorage } from './framework/interfaces/persistence';
import { PouchPersistentStorage } from './infrastructure/pouch.persistence';
import { CHALLENGECRUD } from './infrastructure/challenge-crud';
import { SeedEffect } from '@effects/global/seedEffect';
import { Seeder } from './application/seeders/seeder';
import { ChallengeSeeder } from './application/seeders/challenge.seeder';
import { InfoSeeder } from './application/seeders/info.seeder';
import { WeeklyChallengeSeeder } from './application/seeders/weekly-challenge.seeder';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        StoreModule.forRoot({ global: reducer }),
        EffectsModule.forRoot([ChallengeLoadEffects, InitializerEffect, SaveStateEffect, FirstTimeEffect, SeedEffect]),
        StoreRouterConnectingModule.forRoot({ serializer: DefaultRouterStateSerializer }),
        SharedModule,
        StoreDevtoolsModule.instrument({
            maxAge: 50,
        }),
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ],
    providers: [
        InAppBrowser,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        { provide: ChallengeService, useClass: ChallengePouchService, deps: [CHALLENGECRUD] },
        { provide: PersistentStorage, useClass: PouchPersistentStorage },
        { provide: Seeder, useClass: ChallengeSeeder, multi: true },
        { provide: Seeder, useClass: InfoSeeder, multi: true },
        { provide: Seeder, useClass: WeeklyChallengeSeeder, multi: true },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
