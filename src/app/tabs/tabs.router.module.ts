import { InfoResolverService } from './../services/info-resolver.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'challenge',
                children: [
                    {
                        path: '',
                        loadChildren: '../pages/daily-challenge/challenge.module#ChallengePageModule',
                    },
                ],
            },
            {
                path: 'weekly',
                children: [
                    {
                        path: '',
                        loadChildren: '../pages/weekly-challenge/weekly.module#WeeklyPageModule',
                    },
                ],
            },
            {
                path: 'account',
                children: [
                    {
                        path: '',
                        loadChildren: '../pages/account/account.module#AccountPageModule',
                    },
                ],
            },
            {
                path: '',
                redirectTo: '/tabs/challenge',
                pathMatch: 'full',
            },
        ],
    },
    {
        path: '',
        redirectTo: '/tabs/challenge',
        pathMatch: 'full',
    },
    {
        path: 'info/:id',
        resolve: {
            info: InfoResolverService,
        },
        loadChildren: '../pages/info/info.module#InfoPageModule',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class TabsPageRoutingModule {}
