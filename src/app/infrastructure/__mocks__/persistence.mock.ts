export const persistenceMock = {
    get: jest.fn(),
    put: jest.fn(),
};
