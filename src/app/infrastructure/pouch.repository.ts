import { Identity } from 'app/framework/interfaces/identity';
import { Stringifyable } from 'app/framework/interfaces/stringifyable';
import PouchDB from 'pouchdb';
import pouchFind from 'pouchdb-find';
import { Observable, from, of } from 'rxjs';
import { map, switchMap, catchError, tap } from 'rxjs/operators';
import { Repository } from 'app/framework/interfaces/repository';
import { DbWrapper } from './DbWrapper';

PouchDB.plugin(pouchFind);
function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
    // eslint-disable-next-line id-blacklist
    return value !== null && value !== undefined;
}
export class PouchRepository<T extends Identity<T, TIdentity>, TIdentity extends Stringifyable>
    implements Repository<T, TIdentity> {
    private db: PouchDB.Database<DbWrapper<TIdentity, T>>;

    constructor(name: string) {
        this.db = new PouchDB<DbWrapper<TIdentity, T>>(name);
    }
    public addOrUpdate(val: T): Observable<boolean> {
        return this.add(val).pipe(
            switchMap(success => {
                if (!success) {
                    return this.update(val).pipe(tap(x => console.log('afterUpdate: ' + x)));
                }
                return of(success);
            }),
        );
    }

    public add(val: T): Observable<boolean> {
        const doc = new DbWrapper<TIdentity, T>(val);
        return from(this.db.put(doc)).pipe(
            // tap(x => console.log('during add ')),
            map(_ => true),
            catchError((_, __) => of(false)),
        );
    }

    public update(val: T): Observable<boolean> {
        return from(this.db.get(val.id.toString())).pipe(
            switchMap(w => from(this.db.put(new DbWrapper(val, w._rev)))),
            map(res => res.ok),
        );
    }

    public delete(val: T): Observable<boolean> {
        return from(this.db.get(val.id.toString())).pipe(
            switchMap(w => from(this.db.remove(w))),
            map(res => res.ok),
        );
    }

    public all(): Observable<T[]> {
        // eslint-disable-next-line @typescript-eslint/camelcase
        return from(this.db.allDocs({ include_docs: true })).pipe(
            map(v => v.rows.map(x => x.doc?.value).filter(notEmpty)),
        );
    }

    public get(id: Identity<T, TIdentity>): Observable<T> {
        return from(this.db.get(id.id.toString())).pipe(map(v => v.value));
    }
}
