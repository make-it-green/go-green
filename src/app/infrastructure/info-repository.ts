import { Info } from 'app/domain/entities/info';
import { PouchRepository } from './pouch.repository';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class InfoRepository extends PouchRepository<Info, number> {
    constructor() {
        super('infos');
    }
}
