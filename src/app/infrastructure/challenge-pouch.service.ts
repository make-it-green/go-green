import { ChallengeService } from 'app/application/services/challenge.service';
import { Observable, of, MonoTypeOperatorFunction, OperatorFunction } from 'rxjs';
import { Challenge, Choice } from 'app/domain/entities/challenge';
import { map, switchMap } from 'rxjs/operators';
import { Inject } from '@angular/core';
import { Repository } from 'app/framework/interfaces/repository';
import { CHALLENGECRUD } from './challenge-crud';

export class ChallengePouchService implements ChallengeService {
    constructor(@Inject(CHALLENGECRUD) private repo: Repository<Challenge, number>) {}

    public getChoice(): Observable<Choice> {
        const source = this.repo.all().pipe();
        return source.pipe(
            this.getRandomChallenge(),
            switchMap(challenge =>
                source.pipe(
                    this.filterCategory(challenge.categoryId),
                    this.getRandomChallenge(),
                    switchMap(challenge2 => of([challenge, challenge2] as Choice)),
                ),
            ),
        );
    }

    private filterCategory(categoryId: number): MonoTypeOperatorFunction<Challenge[]> {
        return (source: Observable<Challenge[]>): Observable<Challenge[]> =>
            source.pipe(map(challenges => challenges.filter(challenge => challenge.categoryId !== categoryId)));
    }

    private getRandomChallenge(): OperatorFunction<Challenge[], Challenge> {
        return (source: Observable<Challenge[]>): Observable<Challenge> =>
            source.pipe(map(challenges => challenges[this.getRandomIndex(challenges.length)]));
    }

    private getRandomIndex(length: number): number {
        return Math.floor(Math.random() * length);
    }
}
