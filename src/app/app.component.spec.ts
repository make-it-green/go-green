import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { fakeAsync } from '@angular/core/testing';

import { Platform } from '@ionic/angular';

import { AppComponent } from './app.component';
import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator/jest';
import { Dispatcher } from './framework/dispatcher';
import { MockDirective } from 'ng-mocks';
import { IsloadingDirective } from './framework/directives/IsloadingDirective';
import { initialize } from '@actions/global/initialize';
import { GlobalStateSource } from './framework/sources/globalStateSource';

describe('AppComponent', () => {
    const createComponent = createComponentFactory({
        component: AppComponent,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        mocks: [Platform, Dispatcher, GlobalStateSource],
        declarations: [MockDirective(IsloadingDirective)],
        detectChanges: false,
    });
    let spectator: Spectator<AppComponent>;
    let platformSpy: SpyObject<Platform>;
    let dispatcherSpy: SpyObject<Dispatcher>;
    let platformReadySpy: Promise<string>;

    beforeEach(() => (spectator = createComponent()));

    it('should create the app', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should initialize the app', fakeAsync(() => {
        const app = spectator.component;
        platformSpy = spectator.inject(Platform);
        dispatcherSpy = spectator.inject(Dispatcher);
        platformReadySpy = Promise.resolve('');
        platformSpy.ready.mockReturnValue(platformReadySpy);

        spectator.detectChanges();
        expect(platformSpy.ready).toHaveBeenCalledTimes(1);

        spectator.tick();

        expect(dispatcherSpy.dispatch).toHaveBeenCalledTimes(1);
        expect(dispatcherSpy.dispatch).toHaveBeenCalledWith(initialize());
        expect(app).toBeTruthy();
    }));
});
