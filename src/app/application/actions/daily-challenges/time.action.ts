import { createAction } from '@ngrx/store';

const timeIsNotOver = createAction('[Challenge Daily] TimeIsNotOver');

export default timeIsNotOver;
