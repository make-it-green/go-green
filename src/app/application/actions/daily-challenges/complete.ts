import { createAction, props } from '@ngrx/store';
import { Challenge } from 'app/domain/entities/challenge';

export const complete = createAction('[Challenge Daily] Complete', props<{ challenge: Challenge; startDate: Date }>());

export default complete;
