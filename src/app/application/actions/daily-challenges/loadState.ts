import { createAction } from '@ngrx/store';

export const loadChallengeState = createAction('[Daily-Challenge] LoadState');

export default loadChallengeState;
