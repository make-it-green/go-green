import { createAction, props } from '@ngrx/store';
import { WeeklyChallengeModelState } from 'app/application/reducers/weekly-challenges/weekly-challenge.state';

export const afterLoadedWeeklyChallengeState = createAction(
    '[Weekly-Challenge] AfterLoadedState',
    props<{
        weeklyChallengeState: WeeklyChallengeModelState;
        startDate: Date | null;
    }>(),
);

export default afterLoadedWeeklyChallengeState;
