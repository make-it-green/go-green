import { createAction, props } from '@ngrx/store';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';

export const completeChallenge = createAction(
    '[Weekly-Challenge] Complete',
    props<{ weeklyChallenge: WeeklyChallenge; startDate: Date }>(),
);

export default completeChallenge;
