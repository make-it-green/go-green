import { createAction } from '@ngrx/store';

const timeIsNotOver = createAction('[Weekly-Challenge] TimeIsNotOver');

export default timeIsNotOver;
