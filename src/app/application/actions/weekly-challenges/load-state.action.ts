import { createAction } from '@ngrx/store';

export const loadWeeklyChallengeState = createAction('[Weekly-Challenge] LoadState');

export default loadWeeklyChallengeState;
