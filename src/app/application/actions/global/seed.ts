import { createAction } from '@ngrx/store';

export const seed = createAction('[Global] seed');

export default seed;
