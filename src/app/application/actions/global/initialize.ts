import { createAction } from '@ngrx/store';

export const initialize = createAction('[Global] Init');

export default initialize;
