import { createAction } from '@ngrx/store';

export const afterInitialize = createAction('[Global] AfterInit');

export default afterInitialize;
