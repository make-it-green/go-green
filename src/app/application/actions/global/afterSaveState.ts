import { createAction } from '@ngrx/store';

export const afterSaveState = createAction('[Global] AfterSaveState');

export default afterSaveState;
