import { createAction, props } from '@ngrx/store';
import { Choice } from 'app/domain/entities/challenge';

export const loadedChoice = createAction('[Challenge] LoadedChoice', props<{ choice: Choice; startDate: Date }>());
export default loadedChoice;
