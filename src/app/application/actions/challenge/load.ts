import { createAction, props } from '@ngrx/store';
import { SequentialIdentity } from 'app/framework/interfaces/identity';
import { Challenge } from 'app/domain/entities/challenge';

export const loadChallenge = createAction('[Challenge] Load', props<{ id: SequentialIdentity<Challenge> }>());
export default loadChallenge;
