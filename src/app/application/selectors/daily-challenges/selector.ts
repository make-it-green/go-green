import { createFeatureSelector } from '@ngrx/store';
import { DailyChallengeModelState } from '../../reducers/daily-challenges/daily-challenge.state';

export const dailyChallengeFeature = 'daily-challenge';

export const getDailyChallengeState = createFeatureSelector<DailyChallengeModelState>(dailyChallengeFeature);
