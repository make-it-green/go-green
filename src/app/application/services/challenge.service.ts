import { Choice } from '../../domain/entities/challenge';
import { Observable } from 'rxjs';

export abstract class ChallengeService {
    abstract getChoice(): Observable<Choice>;
}
