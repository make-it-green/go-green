import { WeeklyChallengeService } from './weekly-challenge.service';
import { createServiceFactory } from '@ngneat/spectator';
import { WEEKLYCHALLENGECRUD } from 'app/infrastructure/weekly-challenge.repository';
import { SpectatorService } from '@ngneat/spectator';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';
import { hot } from 'jest-marbles';
import { of } from 'rxjs';

describe('WeeklyChallengeService', () => {
    const weeklyChallengeRepositoryMock = {
        all: jest.fn(),
    };
    const createService = createServiceFactory({
        service: WeeklyChallengeService,
        providers: [{ provide: WEEKLYCHALLENGECRUD, useValue: weeklyChallengeRepositoryMock }],
    });

    let spectator: SpectatorService<WeeklyChallengeService>;

    beforeEach(() => (spectator = createService()));

    it('should be created', () => {
        expect(spectator.service).toBeTruthy();
    });

    it('should be return one weekly challenge', () => {
        const weeklyChallenge = new WeeklyChallenge({
            id: 1,
        });
        weeklyChallengeRepositoryMock.all.mockReturnValue(of([weeklyChallenge]));

        const expected$ = hot('(a|)', {
            a: weeklyChallenge,
        });
        const result$ = spectator.service.getWeeklyChallenge();

        expect(result$).toBeObservable(expected$);
    });
});
