import { hot, cold } from 'jest-marbles';
import { saveState, afterSaveState } from '@actions/global';
import { GlobalModelState } from 'app/application/reducers/global/global.state';
import { provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { SaveStateEffect } from '@effects/global/saveStateEffect';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { PersistentStorage } from 'app/framework/interfaces/persistence';

describe('SaveStateEffect', () => {
    const persistanceStorageMock = {
        put: jest.fn(),
    };
    let actions$: Observable<Action>;
    let effect: SaveStateEffect;
    const initialState = {
        state: 1,
    } as GlobalModelState;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                SaveStateEffect,
                provideMockActions(() => actions$),
                provideMockStore({ initialState }),
                { provide: PersistentStorage, useValue: persistanceStorageMock },
            ],
        });
        effect = TestBed.inject(SaveStateEffect);
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from saveState to afterSaveState ', () => {
        const saveStateAction = saveState();
        const outcomeAfterSaveStateAction = afterSaveState();

        actions$ = hot('-a', { a: saveStateAction });
        const response$ = cold('-b', { b: true });
        persistanceStorageMock.put.mockReturnValue(response$);

        const expected$ = cold('--c', { c: outcomeAfterSaveStateAction });

        expect(effect.stateEffect$).toBeObservable(expected$);
    });
});
