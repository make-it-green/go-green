import { Injectable } from '@angular/core';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';
import { initialize, afterInitialize, firstTime, seed } from '@actions/global';
import { of, merge } from 'rxjs';
import { PersistentStorage } from 'app/framework/interfaces/persistence';

@Injectable()
export class InitializerEffect {
    public loadEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(initialize),
            switchMap(() => this.persistentStorage.get('state')),
            switchMap(state => {
                if (!state) {
                    return of(firstTime());
                }
                return merge(of(seed()), of(afterInitialize()));
            }),
        ),
    );

    constructor(private actions$: Actions, private persistentStorage: PersistentStorage) {}
}
