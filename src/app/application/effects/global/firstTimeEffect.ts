import { Injectable } from '@angular/core';
import { ofType, Actions, Effect } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';
import { initialize, saveState, firstTime } from '@actions/global';
import { merge, of } from 'rxjs';

@Injectable()
export class FirstTimeEffect {
    @Effect()
    public loadEffect$ = this.actions$.pipe(
        ofType(firstTime),
        switchMap(() => merge(of(saveState()), of(initialize()))),
    );

    constructor(private actions$: Actions) {}
}
