import { InfoSeeder } from '../../seeders/info.seeder';
import { initialize } from '@actions/global/initialize';
import { firstTime, saveState } from '@actions/global';
import { ChallengeSeeder } from 'app/application/seeders/challenge.seeder';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of } from 'rxjs';
import { FirstTimeEffect } from '@effects/global/firstTimeEffect';
import { TestBed } from '@angular/core/testing';
import { Action } from '@ngrx/store';
import { hot, cold } from 'jest-marbles';

describe('FirstTimeEffect', () => {
    const seederMock = {
        seed: jest.fn(),
    };
    const infoSeederMock = {
        seed: jest.fn(),
    };
    let actions$: Observable<Action>;
    let effect: FirstTimeEffect;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                FirstTimeEffect,
                provideMockActions(() => actions$),
                { provide: ChallengeSeeder, useValue: seederMock },
                { provide: InfoSeeder, useValue: infoSeederMock },
            ],
        });
        effect = TestBed.inject(FirstTimeEffect);
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from firstTime to saveState and initialize', () => {
        const firstTimeAction = firstTime();
        const outcomeSaveStateAction = saveState();
        const outcomeInitializeAction = initialize();

        actions$ = hot('-a', { a: firstTimeAction });

        const expected$ = cold('-(ab)', {
            a: outcomeSaveStateAction,
            b: outcomeInitializeAction,
        });
        seederMock.seed.mockReturnValue(of(true));

        expect(effect.loadEffect$).toBeObservable(expected$);
    });
});
