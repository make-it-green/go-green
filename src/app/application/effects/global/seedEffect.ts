import { Injectable, Inject } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { Seeder, SeedState } from 'app/application/seeders/seeder';
import seed from '@actions/global/seed';
import { forkJoin, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { afterSeeding } from '@actions/global';
import { Action } from '@ngrx/store';

@Injectable()
export class SeedEffect {
    public seedEffect$ = createEffect(() =>
        this.actions.pipe(
            ofType(seed),
            switchMap(() => this.seedAll()),
        ),
    );

    constructor(private actions: Actions, @Inject(Seeder) private seeder: Seeder[]) {}

    private seedAll(): Observable<Action> {
        return forkJoin(this.seeder.map(seede => seede.seed())).pipe(
            map(success => {
                // console.table(success);
                if (success.some(x => x.seedingResult === SeedState.withErrors)) {
                    throw new Error('Seeding failed!');
                } else {
                    return afterSeeding();
                }
            }),
        );
    }
}
