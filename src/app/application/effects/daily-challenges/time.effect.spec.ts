import { TimeEffect } from './time.effect';
import { PeriodOfTimeCheckerService } from 'app/services/period-of-time-checker.service';
import { TestBed } from '@angular/core/testing';
import { Action } from '@ngrx/store';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of } from 'rxjs';
import { hot, cold } from 'jest-marbles';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { ChallengeHistoryRepository } from 'app/infrastructure/challenge-history.repository';
import { afterLoadState, timeIsNotOver } from '@actions/daily-challenges';
import {
    DailyChallengeModelState,
    DailyChallengeState,
} from 'app/application/reducers/daily-challenges/daily-challenge.state';
import { loadChoice } from '@actions/challenge';
import { dailyChallengeFeature } from '@selectors/daily-challenges/selector';
import { Challenge } from 'app/domain/entities/challenge';

describe('TimeEffect', () => {
    const persistanceStorageMock = {
        get: jest.fn(),
    };
    const historyRepositoryMock = {
        complete: jest.fn(),
        completeChoice: jest.fn(),
    };

    let actions$: Observable<Action>;
    let effect: TimeEffect;
    let periodOfTimeCheckerSpy: jest.SpyInstance<unknown>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                TimeEffect,
                provideMockActions(() => actions$),
                PeriodOfTimeCheckerService,
                { provide: PersistentStorage, useValue: persistanceStorageMock },
                { provide: ChallengeHistoryRepository, useValue: historyRepositoryMock },
            ],
        });
        effect = TestBed.inject(TimeEffect);
        periodOfTimeCheckerSpy = jest.spyOn(TestBed.inject(PeriodOfTimeCheckerService), 'isTimeOver');
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from afterLoadState to loadChoice if startDate is null', () => {
        const dailyChallengeState = new DailyChallengeModelState();
        const afterLoadStateAction = afterLoadState({
            dailyChallengeState,
            startDate: null,
        });
        const loadChoiceAction = loadChoice();

        persistanceStorageMock.get.mockReturnValue(
            of({
                [dailyChallengeFeature]: dailyChallengeState,
            }),
        );

        actions$ = hot('-a', { a: afterLoadStateAction });
        const expected$ = cold('-b', { b: loadChoiceAction });

        expect(effect.timeEffect$).toBeObservable(expected$);
    });

    it('should change action from afterLoadState to loadChoice if time is over', () => {
        const startDate = new Date();
        const dailyChallenge = new Challenge({});
        const dailyChallengeState = new DailyChallengeModelState({
            current: dailyChallenge,
            startDate,
            state: DailyChallengeState.selected,
        });

        const afterLoadStateAction = afterLoadState({
            dailyChallengeState,
            startDate,
        });
        const loadChoiceAction = loadChoice();

        persistanceStorageMock.get.mockReturnValue(
            of({
                [dailyChallengeFeature]: dailyChallengeState,
            }),
        );

        actions$ = hot('-a', { a: afterLoadStateAction });
        const expected$ = cold('-b', { b: loadChoiceAction });

        periodOfTimeCheckerSpy.mockReturnValue(true);

        expect(effect.timeEffect$).toBeObservable(expected$);
    });

    it('should change action from afterLoadState to timeIsNotOver', () => {
        const startDate = new Date();
        const dailyChallenge = new Challenge({});
        const dailyChallengeState = new DailyChallengeModelState({
            current: dailyChallenge,
            startDate,
            state: DailyChallengeState.selected,
        });

        const afterLoadStateAction = afterLoadState({
            dailyChallengeState,
            startDate,
        });
        const timeIsNotOverAction = timeIsNotOver();

        persistanceStorageMock.get.mockReturnValue(
            of({
                [dailyChallengeFeature]: dailyChallengeState,
            }),
        );

        actions$ = hot('-a', { a: afterLoadStateAction });
        const expected$ = cold('-b', { b: timeIsNotOverAction });

        expect(effect.timeEffect$).toBeObservable(expected$);
    });
});
