import { Injectable } from '@angular/core';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';
import { of, merge, Observable } from 'rxjs';
import { getDailyChallengeState } from '@selectors/daily-challenges/selector';
import { loadChallengeState, afterLoadState } from '@actions/daily-challenges';
import { loadChoice } from '@actions/challenge';
import { Action } from '@ngrx/store';

@Injectable()
export class LoadStateEffect {
    public stateEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadChallengeState),
            switchMap(() => this.getState()),
        ),
    );

    constructor(private actions$: Actions, private persistence: PersistentStorage) {}

    private getState(): Observable<Action> {
        return this.persistence.get<object>('state').pipe(
            switchMap((state: object | null) => {
                if (null === state) {
                    return this.getDefaultState();
                }
                return this.getStateFromPersistence(state);
            }),
        );
    }

    private getDefaultState(): Observable<Action> {
        return merge(of(loadChoice()));
    }

    private getStateFromPersistence(state: object): Observable<Action> {
        const dailyChallengeModelState = getDailyChallengeState(state);

        if (null == dailyChallengeModelState || null == dailyChallengeModelState.current) {
            return this.getDefaultState();
        }

        return of(
            afterLoadState({
                dailyChallengeState: dailyChallengeModelState,
                startDate: dailyChallengeModelState.startDate,
            }),
        );
    }
}
