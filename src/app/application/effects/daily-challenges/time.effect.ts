import { createEffect, Actions, ofType } from '@ngrx/effects';
import { PeriodOfTimeCheckerService } from 'app/services/period-of-time-checker.service';
import { ChallengeHistoryRepository } from 'app/infrastructure/challenge-history.repository';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { afterLoadState, selectChoice, timeIsNotOver, complete } from '@actions/daily-challenges';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { loadChoice } from '@actions/challenge';
import { getDailyChallengeState } from '@selectors/daily-challenges/selector';
import {
    DailyChallengeModelState,
    DailyChallengeState,
} from 'app/application/reducers/daily-challenges/daily-challenge.state';
import { Challenge, Choice } from 'app/domain/entities/challenge';
import { HistoryState, ChallengeType } from 'app/domain/enums/history.enum';
import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class TimeEffect {
    public timeEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(afterLoadState, selectChoice, complete),
            switchMap(() => {
                return this.persistence.get<object>('state').pipe(
                    switchMap((state: object | null) => {
                        if (state === null) {
                            return of(null);
                        }
                        const dailyChallengeModelState = getDailyChallengeState(state);
                        return of(dailyChallengeModelState);
                    }),
                );
            }),
            switchMap((dailyChallengeModelState: DailyChallengeModelState | null) => {
                if (null === dailyChallengeModelState) {
                    return of(loadChoice());
                }
                const startDate = dailyChallengeModelState.startDate;

                if (null === startDate) {
                    return of(loadChoice());
                }

                if (this.isTimeOver(startDate)) {
                    this.completeChallenge(dailyChallengeModelState.state, dailyChallengeModelState.current);
                    return of(loadChoice());
                }

                return of(timeIsNotOver());
            }),
        ),
    );

    constructor(
        private actions$: Actions,
        private periodOfTimeChecker: PeriodOfTimeCheckerService,
        private historyRepository: ChallengeHistoryRepository,
        private persistence: PersistentStorage,
    ) {}

    private isTimeOver(startDate: Date): boolean {
        return this.periodOfTimeChecker.isTimeOver(
            new Date(),
            startDate,
            environment.timeTypeForChallenge,
            environment.maxTimeForChallenge,
        );
    }

    private completeChallenge(state: DailyChallengeState | null, dailyChallenge: Challenge | Choice | undefined): void {
        // Ignore challenge if it was completed
        if (state === DailyChallengeState.complete || null == dailyChallenge) {
            return;
        }

        if (Array.isArray(dailyChallenge)) {
            this.historyRepository.completeChoice(dailyChallenge as Choice, ChallengeType.Daily, HistoryState.Failure);
            return;
        }

        this.historyRepository.complete(dailyChallenge, ChallengeType.Daily, HistoryState.Failure);
    }
}
