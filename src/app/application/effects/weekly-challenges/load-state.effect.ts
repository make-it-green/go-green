import { Injectable } from '@angular/core';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';
import { of, merge, Observable } from 'rxjs';
import { loadWeeklyChallengeState, afterLoadedWeeklyChallengeState, loadTeaser } from '@actions/weekly-challenges';
import { Action } from '@ngrx/store';
import { getWeeklyChallengeState } from '@selectors/weekly-challenges/selector';

@Injectable()
export class LoadStateEffect {
    public stateEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadWeeklyChallengeState),
            switchMap(() => this.getState()),
        ),
    );

    constructor(private actions$: Actions, private persistence: PersistentStorage) {}

    private getState(): Observable<Action> {
        return this.persistence.get<object>('state').pipe(
            switchMap((state: object | null) => {
                if (state === null) {
                    return this.getDefaultState();
                }
                return this.getStateFromPersistence(state);
            }),
        );
    }

    private getDefaultState(): Observable<Action> {
        return merge(of(loadTeaser()));
    }

    private getStateFromPersistence(state: object): Observable<Action> {
        const weeklyChallengeModelState = getWeeklyChallengeState(state);

        if (null == weeklyChallengeModelState) {
            return this.getDefaultState();
        }
        const modelState = weeklyChallengeModelState.state;

        if (null == modelState) {
            return this.getDefaultState();
        }
        const startDate = weeklyChallengeModelState.startDate;

        return of(afterLoadedWeeklyChallengeState({ weeklyChallengeState: weeklyChallengeModelState, startDate }));
    }
}
