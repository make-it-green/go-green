import { LoadStateEffect } from './load-state.effect';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { TestBed } from '@angular/core/testing';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { loadWeeklyChallengeState, loadTeaser, afterLoadedWeeklyChallengeState } from '@actions/weekly-challenges';
import { hot, cold } from 'jest-marbles';
import { GlobalState } from 'app/application/reducers/global/global.state';
import {
    WeeklyChallengeModelState,
    WeeklyChallengeState,
} from 'app/application/reducers/weekly-challenges/weekly-challenge.state';
import { weeklyChallengeFeature } from '@selectors/weekly-challenges/selector';

describe('LoadStateEffect', () => {
    const persistanceStorageMock = {
        get: jest.fn(),
    };

    let actions$: Observable<Action>;
    let effect: LoadStateEffect;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                LoadStateEffect,
                provideMockActions(() => actions$),
                { provide: PersistentStorage, useValue: persistanceStorageMock },
            ],
        });
        effect = TestBed.inject(LoadStateEffect);
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from loadWeeklyChallengeState to loadTeaser if state is null', () => {
        const loadWeeklyChallengeStateAction = loadWeeklyChallengeState();
        const loadTeaserAction = loadTeaser();

        actions$ = hot('-a', { a: loadWeeklyChallengeStateAction });
        persistanceStorageMock.get.mockReturnValue(of(null));
        const expected$ = cold('-b', {
            b: loadTeaserAction,
        });

        expect(effect.stateEffect$).toBeObservable(expected$);
    });

    it('should change action from loadWeeklyChallengeState to loadTeaser if weekly not exist in current state', () => {
        const loadWeeklyChallengeStateAction = loadWeeklyChallengeState();
        const loadTeaserAction = loadTeaser();

        actions$ = hot('-a', { a: loadWeeklyChallengeStateAction });
        persistanceStorageMock.get.mockReturnValue(
            of({
                global: GlobalState.initialized,
            }),
        );

        const expected$ = cold('-b', {
            b: loadTeaserAction,
        });

        expect(effect.stateEffect$).toBeObservable(expected$);
    });

    it('should change action from loadWeeklyChallengeState to loadTeaser if weekly state not exist', () => {
        const weeklyChallengeState = new WeeklyChallengeModelState();
        const loadWeeklyChallengeStateAction = loadWeeklyChallengeState();
        const loadTeaserAction = loadTeaser();

        actions$ = hot('-a', { a: loadWeeklyChallengeStateAction });
        persistanceStorageMock.get.mockReturnValue(
            of({
                [weeklyChallengeFeature]: weeklyChallengeState,
                global: GlobalState.initialized,
            }),
        );

        const expected$ = cold('-b', {
            b: loadTeaserAction,
        });

        expect(effect.stateEffect$).toBeObservable(expected$);
    });

    it('should change action from loadWeeklyChallengeState to afterLoadedWeeklyChallengeState', () => {
        const weeklyChallengeState = new WeeklyChallengeModelState(WeeklyChallengeState.teaser);
        const loadWeeklyChallengeStateAction = loadWeeklyChallengeState();
        const loadTeaserAction = afterLoadedWeeklyChallengeState({ weeklyChallengeState, startDate: null });

        actions$ = hot('-a', { a: loadWeeklyChallengeStateAction });
        persistanceStorageMock.get.mockReturnValue(
            of({
                [weeklyChallengeFeature]: weeklyChallengeState,
                global: GlobalState.initialized,
            }),
        );

        const expected$ = cold('-b', {
            b: loadTeaserAction,
        });

        expect(effect.stateEffect$).toBeObservable(expected$);
    });
});
