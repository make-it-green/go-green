import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { WeeklyChallengeService } from 'app/application/services/weekly-challenge.service';
import { loadTeaser, afterLoadedTeaser } from '@actions/weekly-challenges';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class LoadTeaserEffect {
    public stateEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadTeaser),
            switchMap(() => this.weeklyChallengeService.getWeeklyChallenge()),
            switchMap(weeklyChallenge => of(afterLoadedTeaser({ weeklyChallenge, startDate: new Date() }))),
        ),
    );

    constructor(private actions$: Actions, private weeklyChallengeService: WeeklyChallengeService) {}
}
