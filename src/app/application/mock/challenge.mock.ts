import { Challenge } from '../../domain/entities/challenge';
import { Category } from 'app/domain/enums/category';
import { Guid } from 'guid-typescript';

const challengeData = [
    {
        id: 1,
        title: 'Iss kein Fleisch',
        shortDescription:
            'Verzichte heute mal auf Fleisch. Ja, auch Wurst :-P In den tipps findest du leckere Rezepte.',
        categoryId: Category.DIET.id,
        infoId: 1,
        hints: ['Dies ist Tipp Nummer 1.', 'Dies ist Tipp Nummer 2', 'Dies ist Tipp Nummer 3'],
    },
    {
        id: 2,
        title: 'Einmal behältnisse',
        shortDescription:
            'Heute keinen Coffee-To-Go Becher oder Styroporbehälter vom Liefranten, verwenden. Alternativen stehen in den Tipps :-)',
        categoryId: Category.CONSUME.id,
        infoId: 2,
        hints: ['Dies ist Tipp Nummer 1.', 'Dies ist Tipp Nummer 2', 'Dies ist Tipp Nummer 3'],
    },
    {
        id: 3,
        title: 'Heizen/Lüften',
        shortDescription:
            'Heute darf nur effektiv geheizt und gelüftet werden. In den Tipps erfährst du wie das geht! ;-)',
        categoryId: Category.HOUSHOLD.id,
        infoId: 3,
        hints: ['Dies ist Tipp Nummer 1.', 'Dies ist Tipp Nummer 2', 'Dies ist Tipp Nummer 3'],
    },
    {
        id: 4,
        title: 'Auto stehen lassen',
        shortDescription:
            'Lass an mindestens 4 Tagen dein Auto stehen! Für dich und die Umwelt :-) Warum das so wichtig ist, steht in den Tipps.',
        categoryId: Category.MOBILITY.id,
        infoId: 4,
        hints: [
            'Dies ist Tipp Nummer 1',
            'Dies ist Tipp Nummer 2',
            'Dies ist Tipp Nummer 3',
            'Dies ist Tipp Nummer 4',
            'Dies ist Tipp Nummer 5',
        ],
    },
    {
        id: 5,
        title: 'Lies heute ein Buch',
        shortDescription: 'Lies ein Buch zum Thema Nachhaltigkeit',
        categoryId: Category.KNOWLEDGE.id,
        infoId: 5,
        hints: [
            'Dies ist Tipp Nummer 1.',
            'Dies ist Tipp Nummer 2',
            'Dies ist Tipp Nummer 3',
            'Dies ist Tipp Nummer 4',
        ],
    },
];

export function getRandomChallenge(skipIndex: number | null): [Challenge, number] {
    const availableChallengesCount = skipIndex == null ? challengeData.length : challengeData.length - 1;
    let randomChallengeKey = Math.floor(Math.floor(availableChallengesCount) * Math.random());
    if (skipIndex != null && skipIndex <= randomChallengeKey) {
        randomChallengeKey++;
    }
    const randomChallenge = challengeData[randomChallengeKey];

    return [new Challenge(randomChallenge), randomChallengeKey];
}
