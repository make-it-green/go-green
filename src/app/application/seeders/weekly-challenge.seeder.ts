import { Injectable, Inject } from '@angular/core';
import { BaseSeeder } from './base.seeder';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';
import { WEEKLYCHALLENGECRUD } from 'app/infrastructure/weekly-challenge.repository';
import { Repository } from 'app/framework/interfaces/repository';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import weeklyChallengeJson from '../data/weekly-challenges.json';
import { DataMigration } from '../seeders/seeder';

@Injectable({
    providedIn: 'root',
})
export class WeeklyChallengeSeeder extends BaseSeeder<WeeklyChallenge> {
    constructor(
        @Inject(WEEKLYCHALLENGECRUD) weeklyChallengeRepository: Repository<WeeklyChallenge, number>,
        persistence: PersistentStorage,
    ) {
        super(weeklyChallengeJson as DataMigration<WeeklyChallenge>, persistence, weeklyChallengeRepository);
    }
}
