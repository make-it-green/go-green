import { DataMigration } from '../seeders/seeder';
import { InfoRepository } from '../../infrastructure/info-repository';
import { Injectable } from '@angular/core';
import { Info } from 'app/domain/entities/info';
import infoJson from '../data/infos.json';
import { BaseSeeder } from './base.seeder';
import { PersistentStorage } from 'app/framework/interfaces/persistence';

@Injectable({
    providedIn: 'root',
})
export class InfoSeeder extends BaseSeeder<Info> {
    constructor(private infoRepository: InfoRepository, persistence: PersistentStorage) {
        super(infoJson as DataMigration<Info>, persistence, infoRepository);
    }
}
