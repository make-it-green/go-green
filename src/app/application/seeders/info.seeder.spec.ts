import { InfoRepository } from '../../infrastructure/info-repository';
import { InfoSeeder } from './info.seeder';
import { createServiceFactory, SpyObject, SpectatorService } from '@ngneat/spectator/jest';
import { of } from 'rxjs/internal/observable/of';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { SeedState } from '../seeders/seeder';
import { throwError } from 'rxjs';
import { persistenceMock } from 'app/infrastructure/__mocks__/persistence.mock';
jest.mock('../data/infos.json', () => {
    return {
        version: 2,
        versionKey: 'myKey',
        data: [
            {
                id: 0,
                link: 'Test link',
            },
        ],
    };
});

describe('InfoSeeder', () => {
    let spectator: SpectatorService<InfoSeeder>;
    let infoRepositorySpy: SpyObject<InfoRepository>;

    const infoMock = {
        version: 2,
        versionKey: 'myKey',
        data: [
            {
                id: 0,
                link: 'Test link',
            },
        ],
    };

    const createService = createServiceFactory({
        service: InfoSeeder,
        mocks: [InfoRepository],
        providers: [{ provide: PersistentStorage, useValue: persistenceMock }],
    });

    beforeEach(() => {
        spectator = createService();
    });

    it('should be created', () => {
        expect(spectator.service).toBeTruthy();
    });

    it('should add all infos to the database', async () => {
        infoRepositorySpy = spectator.inject(InfoRepository);

        infoRepositorySpy.addOrUpdate.mockReturnValue(of(true));
        infoRepositorySpy.all.mockReturnValue(of([]));
        persistenceMock.get.mockReturnValue(of(1));
        persistenceMock.put.mockReturnValue(of(true));

        const result = await spectator.service.seed();

        expect(result.seedingResult).toEqual(SeedState.success);

        expect(persistenceMock.get).toHaveBeenCalledWith('myKey');
        expect(persistenceMock.put).toHaveBeenCalledWith('myKey', 2);

        expect(infoRepositorySpy.addOrUpdate).toHaveBeenCalledWith(infoMock.data[0]);
        expect(infoRepositorySpy.all).toHaveBeenCalled();
    });

    it('should return false if an error occurs', async () => {
        infoRepositorySpy = spectator.inject(InfoRepository);

        infoRepositorySpy.addOrUpdate.mockReturnValue(throwError('error'));
        infoRepositorySpy.all.mockReturnValue(of([]));

        console.log = jest.fn();
        const result = await spectator.service.seed();
        expect(result.seedingResult).toEqual(SeedState.withErrors);
    });
});
