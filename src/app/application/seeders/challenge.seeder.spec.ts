import { ChallengeSeeder } from 'app/application/seeders/challenge.seeder';
import { SpectatorService } from '@ngneat/spectator';
import { createServiceFactory } from '@ngneat/spectator/jest';
import { of } from 'rxjs/internal/observable/of';
import { SeedState } from '../seeders/seeder';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { CHALLENGECRUD } from 'app/infrastructure/challenge-crud';
import { throwError } from 'rxjs';
import { repositoryMock } from 'app/infrastructure/__mocks__/repository.mock';
import { persistenceMock } from 'app/infrastructure/__mocks__/persistence.mock';
jest.mock('../data/daily-challenges.json', () => {
    return {
        version: 1,
        versionKey: 'mykey',
        data: [
            {
                id: 0,
                title: 'Test title',
                shortDescription: 'Test description',
                categoryId: 1,
                infoId: 1,
            },
        ],
    };
});

describe('ChallengeSeeder', () => {
    let spectator: SpectatorService<ChallengeSeeder>;

    const createService = createServiceFactory({
        service: ChallengeSeeder,
        providers: [
            { provide: CHALLENGECRUD, useValue: repositoryMock },
            { provide: PersistentStorage, useValue: persistenceMock },
        ],
    });

    beforeEach(() => (spectator = createService()));

    it('should be created', () => {
        expect(spectator.service).toBeTruthy();
    });

    it('should add all challenges to the database', async () => {
        persistenceMock.get.mockReturnValue(of(0));
        persistenceMock.put.mockReturnValue(of(true));
        repositoryMock.addOrUpdate.mockReturnValue(of(true));
        repositoryMock.all.mockReturnValue(of([]));
        const result = await spectator.service.seed();
        expect(result.seedingResult).toEqual(SeedState.success);
    });

    it('should return withErrors if an error occurs', async () => {
        persistenceMock.get.mockReturnValue(of(0));
        repositoryMock.addOrUpdate.mockReturnValue(throwError('error'));
        repositoryMock.all.mockReturnValue(of([]));
        console.log = jest.fn();
        const result = await spectator.service.seed();

        expect(result.seedingResult).toEqual(SeedState.withErrors);
    });
});
