import { createReducer, on, Action } from '@ngrx/store';
import { afterInitialize } from '@actions/global';
import { initialState, GlobalModelState, GlobalState } from './global.state';

const rootReducer = createReducer(
    initialState,
    on(afterInitialize, () => new GlobalModelState({ state: GlobalState.initialized })),
);

export function reducer(state: GlobalModelState | undefined, action: Action): GlobalModelState {
    return rootReducer(state, action);
}
