import { SelectedChallengeOrChoice } from 'app/domain/entities/challenge';

export enum DailyChallengeState {
    'loading',
    'selected',
    'choice',
    'complete',
}

export class DailyChallengeModelState {
    current: SelectedChallengeOrChoice | undefined;
    state: DailyChallengeState;
    lastChange: Date;
    startDate: Date | null;

    constructor(
        data: {
            current?: SelectedChallengeOrChoice;
            state?: DailyChallengeState;
            lastChange?: Date;
            startDate?: Date | null;
        } = {},
    ) {
        this.lastChange = data.lastChange ? data.lastChange : new Date();
        this.current = data.current;
        this.state = data.state ? data.state : DailyChallengeState.loading;
        this.startDate = data.startDate ? data.startDate : null;
    }
}

export const initialState: DailyChallengeModelState = new DailyChallengeModelState();

export function getDailyChallengeModelState(): DailyChallengeModelState {
    return new DailyChallengeModelState();
}
