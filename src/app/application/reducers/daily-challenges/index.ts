import { createReducer, on, Action } from '@ngrx/store';
import { DailyChallengeModelState, initialState, DailyChallengeState } from './daily-challenge.state';
import { selectChoice, afterComplete, afterLoadState } from '@actions/daily-challenges';
import { loadChoice, loadedChoice } from '@actions/challenge';

const dailychallengeReducer = createReducer(
    initialState,
    on(loadChoice, () => new DailyChallengeModelState()),
    on(
        loadedChoice,
        (state: DailyChallengeModelState, { choice, startDate }) =>
            new DailyChallengeModelState({ current: choice, state: DailyChallengeState.choice, startDate }),
    ),
    on(
        selectChoice,
        (state: DailyChallengeModelState, { challenge, startDate }) =>
            new DailyChallengeModelState({ current: challenge, state: DailyChallengeState.selected, startDate }),
    ),
    on(
        afterComplete,
        (state: DailyChallengeModelState, { challenge, startDate }) =>
            new DailyChallengeModelState({ current: challenge, state: DailyChallengeState.complete, startDate }),
    ),
    on(
        afterLoadState,
        (state: DailyChallengeModelState, { dailyChallengeState }) => new DailyChallengeModelState(dailyChallengeState),
    ),
);

export function reducer(state: DailyChallengeModelState | undefined, action: Action): DailyChallengeModelState {
    return dailychallengeReducer(state, action);
}
