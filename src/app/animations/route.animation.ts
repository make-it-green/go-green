import { AnimationController, Animation } from '@ionic/angular';
import { Injectable, NgZone } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class RouteAnimation {
    private fadeInAnimation?: Animation;
    private fadeOutAnimation?: Animation;

    constructor(private animationCtrl: AnimationController, private ngZone: NgZone) {}

    initAnimation(elements: undefined | Element[]): void {
        if (elements == null) {
            return;
        }

        this.fadeInAnimation = this.animationCtrl
            .create('fade-out')
            .addElement(elements)
            .duration(700)
            .fromTo('transform', 'scale(0)', 'scale(1)');

        this.fadeOutAnimation = this.animationCtrl
            .create('fade-in')
            .addElement(elements)
            .duration(700)
            .fromTo('transform', 'scale(1)', 'scale(2)')
            .fromTo('opacity', '1', '0');
    }

    playFadeInAnimation(): void {
        if (!this.fadeInAnimation) {
            return;
        }
        this.fadeInAnimation.play();
    }

    playFadeOutAnimation(onFinishCallback: () => void): void {
        if (!this.fadeOutAnimation) {
            return;
        }
        this.fadeOutAnimation.play();
        this.fadeOutAnimation.onFinish(() => {
            this.ngZone.run(onFinishCallback);
        });
    }
}
