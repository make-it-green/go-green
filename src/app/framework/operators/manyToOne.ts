import { Observable, OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';

export function manyToOne(): OperatorFunction<boolean[], boolean> {
    return (source: Observable<boolean[]>): Observable<boolean> => source.pipe(map(all => all.every(r => r)));
}
