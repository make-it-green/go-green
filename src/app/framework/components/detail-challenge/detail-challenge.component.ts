import { Component, Input, ViewChild, AfterViewInit, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { Challenge } from 'app/domain/entities/challenge';
import { IonSlides, AlertController } from '@ionic/angular';
import { Dispatcher } from 'app/framework/dispatcher';
import { Action } from '@ngrx/store';
import { RouteAnimation } from 'app/animations/route.animation';
import { InfoRepository } from 'app/infrastructure/info-repository';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { loadChoice } from '@actions/challenge';

@Component({
    selector: 'app-detail-challenge',
    templateUrl: './detail-challenge.component.html',
    styleUrls: ['./detail-challenge.component.scss'],
})
export class DetailChallengeComponent implements AfterViewInit {
    @ViewChildren('animationElement', { read: ElementRef }) animationElements?: QueryList<ElementRef>;

    @Input() challenge!: Challenge;
    @Input() completeAction!: Action;
    @ViewChild('hintSlides') hintSlides!: IonSlides;

    selectedHintTab = 0;
    slideHintFieldsOpts = {
        initialSlide: this.selectedHintTab,
        speed: 400,
    };

    constructor(
        private dispatcher: Dispatcher,
        private alertController: AlertController,
        private infoRepository: InfoRepository,
        private inAppBrowser: InAppBrowser,
        private routeAnimation: RouteAnimation,
    ) {}

    ngAfterViewInit(): void {
        const elements = this.animationElements?.map(element => element.nativeElement as Element);
        this.routeAnimation.initAnimation(elements);

        this.routeAnimation.playFadeInAnimation();
    }

    finish(): void {
        this.showAlert(this.completeAction, 'Bestätigen', 'Hast du die Challenge erledigt?');
    }

    cancel(): void {
        this.showAlert(
            loadChoice(),
            'Challenge abbrechen?',
            'Bist du dir sicher, dass du die Challenge abbrechen möchtest?',
        );
    }

    async showSource(): Promise<void> {
        const info$ = this.infoRepository.get({ id: this.challenge.infoId });

        await info$.toPromise().then(info => {
            const infoLink = info.link;
            this.inAppBrowser.create(infoLink, '_system', { zoom: 'no' });
        });
    }

    async selectHintTab(id: number): Promise<void> {
        this.selectedHintTab = id;
        await this.hintSlides.slideTo(this.selectedHintTab);
    }

    async swipeHint(): Promise<void> {
        this.selectedHintTab = await this.hintSlides.getActiveIndex();
    }

    private async showAlert(action: Action, header: string, message: string): Promise<void> {
        const alert = await this.alertController.create({
            header,
            message,
            buttons: [
                {
                    text: 'Abbrechen',
                    role: 'Cancel',
                    cssClass: 'button',
                },
                {
                    text: 'Bestätigen',
                    role: 'Ok',
                    handler: (): void => {
                        this.routeAnimation.playFadeOutAnimation((): void => {
                            this.dispatcher.dispatch(action);
                        });
                    },
                },
            ],
        });

        await alert.present();
    }
}
