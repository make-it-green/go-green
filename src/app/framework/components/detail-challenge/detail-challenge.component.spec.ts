import { DetailChallengeComponent } from './detail-challenge.component';
import { CUSTOM_ELEMENTS_SCHEMA, ElementRef } from '@angular/core';
import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator/jest';
import { Dispatcher } from 'app/framework/dispatcher';
import { TransformToCategoryPipe } from 'app/framework/pipes/transform-to-category.pipe';
import { AlertController } from '@ionic/angular';
import { Challenge } from 'app/domain/entities/challenge';
import { RouteAnimation } from 'app/animations/route.animation';
import { InfoRepository } from 'app/infrastructure/info-repository';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Info } from 'app/domain/entities/info';
import { of } from 'rxjs';

describe('DetailChallengeComponent', () => {
    const htmlAlertElementMock = {
        present: jest.fn(),
    };
    const alertControllerMock = {
        create: jest.fn().mockResolvedValue(htmlAlertElementMock),
    };
    const createComponent = createComponentFactory({
        component: DetailChallengeComponent,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        mocks: [Dispatcher, RouteAnimation, InfoRepository, InAppBrowser],
        declarations: [TransformToCategoryPipe],
        providers: [{ provide: AlertController, useValue: alertControllerMock }],
        detectChanges: false,
    });
    let spectator: Spectator<DetailChallengeComponent>;
    let routeAnimationSpy: SpyObject<RouteAnimation>;
    let infoRepositorySpy: SpyObject<InfoRepository>;
    let inAppBrowserSpy: SpyObject<InAppBrowser>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should init animation', () => {
        const challenge = new Challenge({
            id: 1,
            title: 'i am awesome',
            shortDescription: 'i am batman',
            categoryId: 42,
        });
        const app = spectator.component;
        routeAnimationSpy = spectator.inject(RouteAnimation);

        spectator.setInput({
            challenge,
        });
        const abstractElement = spectator.query('#abstract', { read: ElementRef });
        const hintElement = spectator.query('.hints', { read: ElementRef });
        const doneButtonElement = spectator.queryAll('#done_button', { read: ElementRef });
        const closeButtonElement = spectator.queryAll('#close_button', { read: ElementRef });
        const sourceButtonElement = spectator.queryAll('#source_button', { read: ElementRef });

        app.ngAfterViewInit();

        expect(routeAnimationSpy.initAnimation).toHaveBeenCalledWith([
            abstractElement?.nativeElement,
            hintElement?.nativeElement,
            doneButtonElement[0].nativeElement,
            closeButtonElement[0].nativeElement,
            sourceButtonElement[0].nativeElement,
        ]);
        expect(routeAnimationSpy.initAnimation).toHaveBeenCalledTimes(1);
        expect(routeAnimationSpy.playFadeInAnimation).toHaveBeenCalledTimes(1);
    });

    it('should open a modal at click on a challenge', async () => {
        const challenge = new Challenge({
            id: 1,
            title: 'i am awesome',
            shortDescription: 'i am batman',
            categoryId: 42,
        });
        spectator.setInput({ challenge });
        spectator.click('#done_button');

        expect(alertControllerMock.create).toHaveBeenCalledTimes(1);

        const alertElement = await alertControllerMock.create();
        expect(alertElement.present).toHaveBeenCalledTimes(1);
    });

    it('should open extern link', async () => {
        const challenge = new Challenge({
            id: 1,
            infoId: 1,
        });
        const info$ = new Info({
            id: 1,
            link: 'link.test',
        });
        spectator.setInput({ challenge });

        infoRepositorySpy = spectator.inject(InfoRepository);
        inAppBrowserSpy = spectator.inject(InAppBrowser);

        infoRepositorySpy.get.mockReturnValue(of(info$));

        await spectator.component.showSource();

        expect(infoRepositorySpy.get).toHaveBeenCalledTimes(1);
        expect(inAppBrowserSpy.create).toHaveBeenCalledTimes(1);
        expect(inAppBrowserSpy.create).toHaveBeenCalledWith('link.test', '_system', { zoom: 'no' });
    });
});
