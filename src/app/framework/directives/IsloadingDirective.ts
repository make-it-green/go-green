import {
    Directive,
    Input,
    ViewContainerRef,
    TemplateRef,
    ComponentFactoryResolver,
    ComponentFactory,
    EmbeddedViewRef,
    ComponentRef,
} from '@angular/core';
import { IonSpinner } from '@ionic/angular';
import { IsLoadingContext } from './isloadingContext';

@Directive({
    selector: '[appIsloading]',
})
export class IsloadingDirective<T = unknown> {
    @Input()
    class = 'center';
    private context: IsLoadingContext<T> = new IsLoadingContext();
    private factory: ComponentFactory<IonSpinner>;
    private childView: EmbeddedViewRef<IsLoadingContext<T>> | null = null;
    private loadView: ComponentRef<IonSpinner> | null = null;
    @Input()
    set appIsloading(val: T) {
        this.context.appIsloading = val;
        this.updateView();
    }

    constructor(
        private templateRef: TemplateRef<IsLoadingContext<T>>,
        private viewContainer: ViewContainerRef,
        resolver: ComponentFactoryResolver,
    ) {
        this.factory = resolver.resolveComponentFactory(IonSpinner);
    }

    private updateView(): void {
        if (this.isChildViewCreated()) {
            return;
        }
        this.viewContainer.clear();

        if (this.contextExist()) {
            this.createChildView();
            return;
        }
        this.createLoadingSpinner();
    }

    private isChildViewCreated(): boolean {
        return null !== this.childView;
    }

    private contextExist(): boolean {
        return null !== this.context.appIsloading;
    }

    private createChildView(): void {
        this.loadView = null;
        this.childView = this.viewContainer.createEmbeddedView(this.templateRef, this.context);
    }

    private createLoadingSpinner(): void {
        this.childView = null;
        this.loadView = this.viewContainer.createComponent(this.factory);
        this.loadView.instance.name = 'bubbles';
    }
}
