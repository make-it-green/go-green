import { Injectable } from '@angular/core';
import { GlobalModelState } from 'app/application/reducers/global/global.state';
import { Store } from '@ngrx/store';
import { GlobalSource } from './globalSource';

@Injectable({
    providedIn: 'root',
})
export class GlobalStateSource extends GlobalSource<GlobalModelState> {
    constructor(private store: Store<GlobalModelState>) {
        super(store, source => source);
    }
}
