import { Source } from '../interfaces/source';
import { Observable, OperatorFunction } from 'rxjs';
import { Store, Selector } from '@ngrx/store';
import { GlobalModelState } from 'app/application/reducers/global/global.state';

export class FeatureSource<TState, TOut> implements Source<TOut> {
    public out$: Observable<TOut>;

    constructor(
        store: Store<GlobalModelState>,
        operator: OperatorFunction<TState, TOut>,
        selector: Selector<GlobalModelState, TState>,
    ) {
        this.out$ = store.select(selector).pipe(operator);
    }
}
