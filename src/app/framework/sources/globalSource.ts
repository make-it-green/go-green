import { Source } from '../interfaces/source';
import { Observable, OperatorFunction } from 'rxjs';
import { Store } from '@ngrx/store';

export class GlobalSource<TOut> implements Source<TOut> {
    public out$: Observable<TOut>;

    constructor(store: Store<any>, operator: OperatorFunction<any, TOut>) {
        this.out$ = store.select(m => m.global).pipe(operator);
    }
}
