import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export abstract class PersistentStorage {
    public abstract put<T>(key: string, val: T): Observable<boolean>;
    public abstract get<T>(key: string): Observable<T | null>;
}
