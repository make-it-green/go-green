import { Observable } from 'rxjs';
import { Identity } from 'app/framework/interfaces/identity';
import { InjectionToken } from '@angular/core';
import { Stringifyable } from './stringifyable';

export interface Repository<T extends Identity<T, TIdentity>, TIdentity extends Stringifyable> {
    addOrUpdate(val: T): Observable<boolean>;
    add(val: T): Observable<boolean>;
    update(val: T): Observable<boolean>;
    delete(val: T): Observable<boolean>;
    all(): Observable<T[]>;
    get(id: Identity<T, TIdentity>): Observable<T>;
}

export function createRepositoryToken<
    TIdentity extends Stringifyable,
    T extends Identity<T, TIdentity>,
    TRepository extends Repository<T, TIdentity>
>(key: string, factory: () => TRepository): InjectionToken<TRepository> {
    return new InjectionToken(key, {
        providedIn: 'root',
        factory,
    });
}
