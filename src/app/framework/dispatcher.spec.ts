import { TestBed } from '@angular/core/testing';
import { Dispatcher } from './dispatcher';
import { provideMockStore } from '@ngrx/store/testing';
import { createAction, Store } from '@ngrx/store';

describe('Dispatcher', () => {
    beforeEach(() =>
        TestBed.configureTestingModule({
            providers: [provideMockStore()],
        }),
    );

    it('should be created', () => {
        const service: Dispatcher = TestBed.inject(Dispatcher);
        expect(service).toBeTruthy();
    });

    it('should call dispatch', () => {
        const service: Dispatcher = TestBed.inject(Dispatcher);
        const store: Store<{}> = TestBed.inject(Store);
        const spy = spyOn(store, 'dispatch');
        service.dispatch(createAction('test'));
        // eslint-disable-next-line @typescript-eslint/unbound-method
        expect(store.dispatch).toHaveBeenCalled();
    });
});
