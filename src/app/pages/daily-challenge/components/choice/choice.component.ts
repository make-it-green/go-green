import { Category } from 'app/domain/enums/category';
import { Component, Input, ElementRef, ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import { Challenge } from 'app/domain/entities/challenge';
import { Dispatcher } from 'app/framework/dispatcher';
import { selectChoice } from '@actions/daily-challenges';
import { RouteAnimation } from 'app/animations/route.animation';

@Component({
    selector: 'app-choice',
    templateUrl: './choice.component.html',
    styleUrls: ['./choice.component.scss'],
})
export class ChoiceComponent implements AfterViewInit {
    @ViewChildren('animationElement', { read: ElementRef }) animationElements?: QueryList<ElementRef>;

    @Input() choices: [Challenge, Challenge] | undefined;
    @Input() startDate!: Date;

    selectedChallenge: Challenge = new Challenge({});
    chosenOne = false;
    categories: Category[] = Category.categories;

    constructor(private dispatcher: Dispatcher, private routeAnimation: RouteAnimation) {}

    ngAfterViewInit(): void {
        const elements = this.animationElements?.map(element => element.nativeElement as Element);

        this.routeAnimation.initAnimation(elements);
        this.routeAnimation.playFadeInAnimation();
    }

    select(challenge: Challenge): void {
        if (challenge === this.selectedChallenge) {
            this.routeAnimation.playFadeOutAnimation((): void => {
                const selectChoiceAction = selectChoice({ challenge, startDate: this.startDate });
                this.dispatcher.dispatch(selectChoiceAction);
            });
        } else {
            this.selectedChallenge = challenge;
        }
    }
}
