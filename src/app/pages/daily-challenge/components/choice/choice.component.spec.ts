import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator/jest';
import { CUSTOM_ELEMENTS_SCHEMA, ElementRef } from '@angular/core';
import { ChoiceComponent } from './choice.component';
import { Challenge } from 'app/domain/entities/challenge';
import { Dispatcher } from 'app/framework/dispatcher';
import { TransformToCategoryPipe } from 'app/framework/pipes/transform-to-category.pipe';
import { RouteAnimation } from 'app/animations/route.animation';

describe('ChoiceComponent', () => {
    const createComponent = createComponentFactory({
        component: ChoiceComponent,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        mocks: [Dispatcher, RouteAnimation],
        declarations: [TransformToCategoryPipe],
        detectChanges: false,
    });
    let spectator: Spectator<ChoiceComponent>;
    let routeAnimationSpy: SpyObject<RouteAnimation>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should init animation', () => {
        const challenge = new Challenge({
            id: 1,
            title: 'i am awesome',
            shortDescription: 'i am batman',
            categoryId: 42,
        });
        const challenge2 = new Challenge({
            id: 2,
            title: 'i am awesome too',
            shortDescription: 'a am batman too?',
            categoryId: 42,
        });
        const app = spectator.component;
        routeAnimationSpy = spectator.inject(RouteAnimation);

        spectator.setInput({
            choices: [challenge, challenge2],
        });
        const choiceElements = spectator.queryAll('ion-card', { read: ElementRef });

        app.ngAfterViewInit();

        expect(routeAnimationSpy.initAnimation).toHaveBeenCalledWith([
            choiceElements[0].nativeElement,
            choiceElements[1].nativeElement,
        ]);
        expect(routeAnimationSpy.initAnimation).toHaveBeenCalledTimes(1);
        expect(routeAnimationSpy.playFadeInAnimation).toHaveBeenCalledTimes(1);
    });

    it('should show 2 challenges', () => {
        const challenge = new Challenge({
            id: 1,
            title: 'i am awesome',
            shortDescription: 'i am batman',
            categoryId: 42,
        });
        const challenge2 = new Challenge({
            id: 2,
            title: 'i am awesome too',
            shortDescription: 'a am batman too?',
            categoryId: 42,
        });

        spectator.setInput({
            choices: [challenge, challenge2],
        });

        const choiceElements = spectator.queryAll('ion-card');
        expect(choiceElements.length).toBe(2);
    });

    it('should animate if clicked twice on a challenge', () => {
        const startDate = new Date();
        const challenge = new Challenge({
            id: 1,
            title: 'i am awesome',
            shortDescription: 'i am batman',
            categoryId: 42,
        });
        const challenge2 = new Challenge({
            id: 2,
            title: 'i am awesome too',
            shortDescription: 'a am batman too?',
            categoryId: 42,
        });

        routeAnimationSpy = spectator.inject(RouteAnimation);

        spectator.setInput({
            choices: [challenge, challenge2],
            startDate,
        });

        const choiceElements = spectator.queryAll('ion-card');
        spectator.click(choiceElements[0]);
        spectator.click(choiceElements[0]);

        expect(routeAnimationSpy.playFadeOutAnimation).toHaveBeenCalledTimes(1);
    });
});
