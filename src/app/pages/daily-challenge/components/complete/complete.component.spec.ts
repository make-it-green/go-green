import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator/jest';
import { CUSTOM_ELEMENTS_SCHEMA, ElementRef } from '@angular/core';

import { CompleteComponent } from './complete.component';
import { Dispatcher } from 'app/framework/dispatcher';
import { RouteAnimation } from 'app/animations/route.animation';
import { TransformToCategoryPipe } from 'app/framework/pipes/transform-to-category.pipe';
import { Challenge } from 'app/domain/entities/challenge';

describe('CompleteComponent', () => {
    const createComponent = createComponentFactory({
        component: CompleteComponent,
        mocks: [Dispatcher, RouteAnimation],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        declarations: [TransformToCategoryPipe],
        detectChanges: false,
    });
    let spectator: Spectator<CompleteComponent>;
    let routeAnimationSpy: SpyObject<RouteAnimation>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should init animation', () => {
        const app = spectator.component;
        const challenge = new Challenge({
            id: 1,
            title: 'i am awesome',
            shortDescription: 'i am batman',
            successMessage: 'success message',
            categoryId: 42,
        });
        routeAnimationSpy = spectator.inject(RouteAnimation);

        spectator.setInput({
            challenge,
        });

        const contentElement = spectator.query('ion-card-content', { read: ElementRef });
        const cardTitleElement = spectator.query('ion-card-title', { read: ElementRef });
        const buttonElement = spectator.query('ion-button', { read: ElementRef });

        app.ngAfterViewInit();

        expect(routeAnimationSpy.initAnimation).toHaveBeenCalledWith([
            cardTitleElement?.nativeElement,
            contentElement?.nativeElement,
            buttonElement?.nativeElement,
        ]);
        expect(routeAnimationSpy.initAnimation).toHaveBeenCalledTimes(1);
    });

    it('should animate on a retry button', () => {
        const retryButton = spectator.queryAll('ion-button');
        spectator.click(retryButton[0]);

        routeAnimationSpy = spectator.inject(RouteAnimation);

        expect(routeAnimationSpy.playFadeOutAnimation).toHaveBeenCalledTimes(1);
    });
});
