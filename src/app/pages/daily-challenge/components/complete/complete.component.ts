import { Component, Input, AfterViewInit, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { Dispatcher } from 'app/framework/dispatcher';
import { loadChoice } from '@actions/challenge';
import { RouteAnimation } from 'app/animations/route.animation';
import { Challenge } from 'app/domain/entities/challenge';

// TODO mach das schön Stefan
@Component({
    selector: 'app-complete',
    templateUrl: './complete.component.html',
    styleUrls: ['./complete.component.scss'],
})
export class CompleteComponent implements AfterViewInit {
    @ViewChildren('animationElement', { read: ElementRef }) animationElements?: QueryList<ElementRef>;

    @Input() challenge?: Challenge;

    constructor(private dispatcher: Dispatcher, private routeAnimation: RouteAnimation) {}

    ngAfterViewInit(): void {
        const elements = this.animationElements?.map(element => element.nativeElement as Element);

        this.routeAnimation.initAnimation(elements);
        this.routeAnimation.playFadeInAnimation();
    }

    retry(): void {
        this.routeAnimation.playFadeOutAnimation((): void => {
            this.dispatcher.dispatch(loadChoice());
        });
    }
}
