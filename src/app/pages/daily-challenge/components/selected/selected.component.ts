import { Component, OnInit, Input } from '@angular/core';
import { Challenge } from 'app/domain/entities/challenge';
import { Action } from '@ngrx/store';
import { complete } from '@actions/daily-challenges';

@Component({
    selector: 'app-selected',
    templateUrl: './selected.component.html',
    styleUrls: ['./selected.component.scss'],
})
export class SelectedComponent implements OnInit {
    @Input() challenge!: Challenge;
    @Input() startDate!: Date;

    constructor() {}

    ngOnInit(): void {}

    getCompleteAction(): Action {
        return complete({ challenge: this.challenge, startDate: this.startDate });
    }
}
