import { OperatorFunction } from 'rxjs';
import { DailyChallengeModelState } from 'app/application/reducers/daily-challenges/daily-challenge.state';
import { DailyChallengeViewModel } from '../viewModel/daily-challenge.viewmodel';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { GlobalModelState } from 'app/application/reducers/global/global.state';
import { getDailyChallengeState } from '@selectors/daily-challenges/selector';
import { FeatureSource } from 'app/framework/sources/featureSource';

const mapViewModel: OperatorFunction<DailyChallengeModelState, DailyChallengeViewModel> = source =>
    source.pipe(map(state => new DailyChallengeViewModel(state)));

@Injectable()
export class DailyChallengeSource extends FeatureSource<DailyChallengeModelState, DailyChallengeViewModel> {
    constructor(store: Store<GlobalModelState>) {
        super(store, mapViewModel, getDailyChallengeState);
    }
}
