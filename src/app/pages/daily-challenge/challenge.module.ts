import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ChallengePage } from './challenge.page';

import { StoreModule } from '@ngrx/store';
import { reducer } from '../../application/reducers/daily-challenges';
import { ChoiceComponent } from './components/choice/choice.component';
import { SelectedComponent } from './components/selected/selected.component';
import { dailyChallengeFeature } from 'app/application/selectors/daily-challenges/selector';
import { EffectsModule } from '@ngrx/effects';
import { ChoiceLoadEffect } from 'app/application/effects/daily-challenges/loadChoiceEffect';
import { CompleteEffect } from 'app/application/effects/daily-challenges/completeEffect';
import { CompleteComponent } from './components/complete/complete.component';
import { DailyChallengeSource } from './sources/daily-challenge.source';
import { SharedModule } from 'app/framework/shared/shared.module';
import { LoadStateEffect } from '@effects/daily-challenges/loadStateEffect';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        SharedModule,
        RouterModule.forChild([{ path: '', component: ChallengePage }]),
        EffectsModule.forFeature([CompleteEffect, ChoiceLoadEffect, LoadStateEffect]),
        StoreModule.forFeature(dailyChallengeFeature, reducer),
    ],
    providers: [DailyChallengeSource],
    declarations: [ChallengePage, ChoiceComponent, SelectedComponent, CompleteComponent],
})
export class ChallengePageModule {}
