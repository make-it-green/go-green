import { TeaserComponent } from './teaser.component';
import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Dispatcher } from 'app/framework/dispatcher';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';
import { loadActiveChallenge } from '@actions/weekly-challenges';
import { TransformToCategoryPipe } from 'app/framework/pipes/transform-to-category.pipe';

describe('TeaserComponent', () => {
    const createComponent = createComponentFactory({
        component: TeaserComponent,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        mocks: [Dispatcher],
        declarations: [TransformToCategoryPipe],
        detectChanges: false,
    });
    let spectator: Spectator<TeaserComponent>;
    let dispatcherSpy: SpyObject<Dispatcher>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should throw an error if input value challenge is not defined', () => {
        spectator.setInput({ startDate: new Date() });
        expect(() => spectator.detectChanges()).toThrowError('The input "challenge" is required');
    });

    it('should throw an error if input value date is not defined', () => {
        const challenge = new WeeklyChallenge({});
        spectator.setInput({ challenge });
        expect(() => spectator.detectChanges()).toThrowError('The input "startDate" is required');
    });

    it('should dispatch a loadActiveChallenge action if twice clicked on element', () => {
        const challenge = new WeeklyChallenge({});
        const startDate = new Date();

        const loadActiveChallengeAction = loadActiveChallenge({ weeklyChallenge: challenge, startDate });

        dispatcherSpy = spectator.inject(Dispatcher);

        spectator.setInput({ challenge, startDate });

        const circleElement = spectator.queryAll('.circle');
        spectator.click(circleElement[0]);
        spectator.click(circleElement[0]);

        expect(dispatcherSpy.dispatch).toHaveBeenCalledTimes(1);
        expect(dispatcherSpy.dispatch).toHaveBeenCalledWith(loadActiveChallengeAction);
    });
});
