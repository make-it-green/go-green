import { WeeklyPage } from './weekly.page';
import { TeaserComponent } from './components/teaser/teaser.component';
import { ActiveComponent } from './components/active/active.component';
import { CompleteComponent } from './components/complete/complete.component';
import { WeeklyChallengeSource } from './sources/weekly.source';
import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Dispatcher } from 'app/framework/dispatcher';
import { MockDirective, MockComponent } from 'ng-mocks';
import { IsloadingDirective } from 'app/framework/directives/IsloadingDirective';
import { loadWeeklyChallengeState } from '@actions/weekly-challenges';

describe('WeeklyPage', () => {
    const createComponent = createComponentFactory({
        component: WeeklyPage,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        mocks: [WeeklyChallengeSource, Dispatcher],
        declarations: [
            MockDirective(IsloadingDirective),
            MockComponent(TeaserComponent),
            MockComponent(ActiveComponent),
            MockComponent(CompleteComponent),
        ],
        detectChanges: false,
    });
    let spectator: Spectator<WeeklyPage>;
    let dispatcherSpy: SpyObject<Dispatcher>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should dispatch loadWeeklyChallengeState at ionViewWillEnter', () => {
        const app = spectator.component;
        dispatcherSpy = spectator.inject(Dispatcher);

        app.ionViewWillEnter();

        expect(dispatcherSpy.dispatch).toHaveBeenCalledTimes(1);
        expect(dispatcherSpy.dispatch).toHaveBeenCalledWith(loadWeeklyChallengeState());
    });
});
