import {
    WeeklyChallengeState,
    WeeklyChallengeModelState,
} from 'app/application/reducers/weekly-challenges/weekly-challenge.state';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';

export class WeeklyChallengeViewModel {
    state?: WeeklyChallengeState | null;
    current?: WeeklyChallenge | null;
    startDate?: Date | null;
    lastAction: Date;
    isLoading: boolean;

    constructor(weeklyChallengeModelState: WeeklyChallengeModelState) {
        this.lastAction = weeklyChallengeModelState.lastChange;
        this.startDate = weeklyChallengeModelState.startDate;
        this.current = weeklyChallengeModelState.current;
        this.state = weeklyChallengeModelState.state;

        if (this.state) {
            this.isLoading = false;
        } else {
            this.isLoading = true;
        }
    }
}
