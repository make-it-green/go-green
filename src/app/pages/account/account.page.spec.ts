import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator/jest';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AccountPage } from './account.page';
import { ChallengeHistoryRepository } from 'app/infrastructure/challenge-history.repository';
import { of } from 'rxjs';
import { Challenge } from 'app/domain/entities/challenge';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';
import { ChallengeCounter } from './models/challenge-counter.model';
import { hot } from 'jest-marbles';
import { Category } from 'app/domain/enums/category';

describe('AccountComponent', () => {
    const createComponent = createComponentFactory({
        component: AccountPage,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        mocks: [ChallengeHistoryRepository],
        detectChanges: false,
    });
    let spectator: Spectator<AccountPage>;
    let challengeHistoryRepositorySpy: SpyObject<ChallengeHistoryRepository>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should create mixed challengeCounter', () => {
        const app = spectator.component;
        const weeklyChallenge1 = new WeeklyChallenge({});
        const weeklyChallenge2 = new WeeklyChallenge({});
        const consumeDailyChallenge = new Challenge({ categoryId: Category.CONSUME.id });
        const dietDailyChallenge = new Challenge({ categoryId: Category.DIET.id });
        const completedChallenges = [
            [consumeDailyChallenge, dietDailyChallenge],
            [weeklyChallenge1, weeklyChallenge2],
        ] as [Challenge[], WeeklyChallenge[]];
        const challengeCounter = new ChallengeCounter();
        challengeCounter.completedWeeklyChallenge = 3;
        challengeCounter.completedConsumeChallengeCount = 1;
        challengeCounter.completedDietChallengeCount = 1;
        const expected$ = hot('(a|)', { a: challengeCounter });
        challengeHistoryRepositorySpy = spectator.inject(ChallengeHistoryRepository);

        challengeHistoryRepositorySpy.getCompletedChallenges.mockReturnValue(of(completedChallenges));
        app.ionViewWillEnter();

        expect(app.challengeCounter$).toBeObservable(expected$);
    });

    it('should create empty challengeCounter', () => {
        const app = spectator.component;
        const completedChallenges = [[], []] as [Challenge[], WeeklyChallenge[]];
        const challengeCounter = new ChallengeCounter();
        const expected$ = hot('(a|)', { a: challengeCounter });
        challengeHistoryRepositorySpy = spectator.inject(ChallengeHistoryRepository);

        challengeHistoryRepositorySpy.getCompletedChallenges.mockReturnValue(of(completedChallenges));
        app.ionViewWillEnter();

        expect(app.challengeCounter$).toBeObservable(expected$);
    });
});
