import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfoPage } from './info.page';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/framework/shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedModule,
        RouterModule.forChild([{ path: '', component: InfoPage }]),
    ],
    declarations: [InfoPage],
})
export class InfoPageModule {}
