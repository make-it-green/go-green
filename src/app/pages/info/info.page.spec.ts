import { Info } from 'app/domain/entities/info';

import { InfoPage } from './info.page';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

describe('InfoPage', () => {
    const routeMock = {
        snapshot: {
            data: {
                info: new Info({ id: 1, link: 'Test link' }),
            },
        },
    };
    const createComponent = createComponentFactory({
        component: InfoPage,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [{ provide: ActivatedRoute, useValue: routeMock }],
        detectChanges: false,
    });
    let spectator: Spectator<InfoPage>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should define info', () => {
        const app = spectator.component;

        spectator.detectChanges();

        expect(app.info).toBe(routeMock.snapshot.data.info);
        expect(app).toBeTruthy();
    });
});
