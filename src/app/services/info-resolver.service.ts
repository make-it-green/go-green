import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { InfoRepository } from 'app/infrastructure/info-repository';
import { Info } from 'app/domain/entities/info';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
    providedIn: 'root',
})
export class InfoResolverService implements Resolve<Info> {
    constructor(private infoRepository: InfoRepository) {}

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        const infoId = route.paramMap.get('id');

        if (null !== infoId) {
            return this.infoRepository.get({ id: parseInt(infoId, 10) });
        }
        return of(null);
    }
}
