import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root',
})
export class ErrorService {
    constructor(private toastController: ToastController) {}

    async showMessage(message: string): Promise<void> {
        const toast = await this.toastController.create({ message, duration: 2000 });
        toast.present();
    }
}
