import { Info } from './../domain/entities/info';
import { cold } from 'jest-marbles';
import { of } from 'rxjs';

import { InfoResolverService } from './info-resolver.service';
import { SpectatorService, createServiceFactory, SpyObject } from '@ngneat/spectator/jest';
import { ActivatedRouteSnapshot } from '@angular/router';
import { InfoRepository } from 'app/infrastructure/info-repository';

describe('InfoResolverService', () => {
    const paramMapMock = {
        get: jest.fn(),
    };
    const routeMock = {
        get paramMap(): any {
            return paramMapMock;
        },
    };

    let spectator: SpectatorService<InfoResolverService>;
    let infoRepository: SpyObject<InfoRepository>;

    const createService = createServiceFactory({
        service: InfoResolverService,
        mocks: [InfoRepository],
        providers: [{ provide: ActivatedRouteSnapshot, useValue: routeMock }],
    });

    beforeEach(() => (spectator = createService()));

    it('should be created', () => {
        expect(spectator.service).toBeTruthy();
    });

    it('should return null if info not exist', () => {
        const route = spectator.inject(ActivatedRouteSnapshot);
        const expectedResult = cold('(a|)', { a: null });

        paramMapMock.get.mockReturnValue(null);

        expect(spectator.service.resolve(route)).toBeObservable(expectedResult);
    });

    it('should return info', () => {
        const route = spectator.inject(ActivatedRouteSnapshot);
        const info = new Info({ id: 1, link: 'Test link' });

        infoRepository = spectator.inject(InfoRepository);

        paramMapMock.get.mockReturnValue(1);
        infoRepository.get.mockReturnValue(of(info));

        const expectedResult = cold('(a|)', { a: info });
        expect(spectator.service.resolve(route)).toBeObservable(expectedResult);
    });
});
