import { SequentialIdentity } from 'app/framework/interfaces/identity';

export class Info implements SequentialIdentity<Info> {
    id: number;
    link: string;

    constructor(data: Required<Info>) {
        this.id = data.id;
        this.link = data.link;
    }
}
