import { RandomIdentity } from 'app/framework/interfaces/identity';
import { HistoryState, ChallengeType } from '../enums/history.enum';
import { Guid } from 'guid-typescript';

export class ChallengeHistory implements RandomIdentity<ChallengeHistory> {
    public id: string;

    constructor(
        public challengeId: number,
        public type: ChallengeType,
        public state: HistoryState,
        public finishedAt: Date,
    ) {
        this.id = Guid.create().toString();
    }
}
