import { TestBed } from '@angular/core/testing';
import { Challenge } from './challenge';
import { Guid } from 'guid-typescript';

describe('Challenge', () => {
    beforeEach(() =>
        TestBed.configureTestingModule({
            providers: [],
        }),
    );
    it('should create', () => {
        const challenge = new Challenge({});
        expect(challenge).toBeTruthy();
        expect(challenge.id).toBe(0);
        expect(challenge.title).toBe('');
        expect(challenge.shortDescription).toBe('');
        expect(challenge.successMessage).toBe('');
        expect(challenge.categoryId).toBe(0);
    });
});
