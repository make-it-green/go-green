import { browser, by, element, promise as prPromise } from 'protractor';

export class AppPage {
  navigateTo(): prPromise.Promise<any> {
    return browser.get('/');
  }

  getPageTitle(): prPromise.Promise<string> {
    return element(by.css('ion-title')).getText();
  }
}
