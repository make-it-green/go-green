import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
    appId: 'io.ionic.starter',
    appName: 'go-green',
    webDir: 'www',
    bundledWebRuntime: true,
};

export default config;
