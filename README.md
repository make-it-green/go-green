# Go Green

[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/make-it-green/go-green/master?style=for-the-badge)](https://gitlab.com/make-it-green/go-green/-/pipelines)

Project to develop an app to improve the carbon footprint.

## Installation

Install Apache Cardova, which will take your app and bundle it into a native wrapper
to turn it into a traditional native app.

    yarn global add cordova

Install Ionic to start and build Ionic apps

    yarn global add ionic

Install all necessary packages for the project

    yarn install

Start ionic

    ionic serve

## Android local development

First, install Ionic DevApp from play store.
Here is a link to the app https://play.google.com/store/apps/details?id=io.ionic.devapp&hl=en

Then run the following command on your computer

    ionic serve --devapp

Next, open your Android device and connect to the same network as your computer (through Wi-Fi).
Open the DevApp, and you should see your local app show up in the list.

## Eslint and Prettier

Run the following command to find and fix syntax errors

    yarn run lint:fix

If you use Vscode then install ESlint and Prettier - Code formatter.
Look in Preferences/Settings/Extensions for 'formatOnSave' and check the checkbox.
Now your code will be formatted based on the prettierrc.js config

## Debug configuration in Visual Studio

We need to initially configure the debugger. To do so, go to the Run view and click on the
gear button to create a launch.json debugger configuration file. Choose Chrome from the
Select Environment drop-down list. This will create a launch.json file in a new .vscode
folder in your project which includes a configuration to launch the website.

Configuration to debug the app:

    {
        "name": "Launch Chrome against localhost",
        "type": "chrome",
        "request": "launch",
        "url": "http://localhost:8100",
        "webRoot": "${workspaceFolder}"
    }

Configuration to debug the jest tests:

    {
        "name": "Debug Jest Tests",
        "type": "node",
        "request": "launch",
        "runtimeArgs": ["--inspect-brk", "${workspaceRoot}/node_modules/.bin/jest", "--runInBand"],
        "console": "integratedTerminal",
        "internalConsoleOptions": "neverOpen",
        "port": 9229
    }
