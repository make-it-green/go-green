# Technologies

## Organization

### GitLab

We use GitLab as a remote storage and collaboration platform for git as our tool for version control. Furthermore, the [CI/CD-tools](https://gitlab.com/make-it-green/go-green/-/pipelines) are useful to instantly check whether the most recent changes are buildable and the linter won't complain. A nice side-effect of this feature is we get fresh android applications to test them on real devices.

### Trello

To efficiently manage our tasks, we use a [Trello-Board](https://trello.com/b/88On8CuK/forward).

## Build System

### Docker

In order to have reproducible (android) builds, we use Docker.

### Yarn

We use Yarn as our package manager as it is remarkably stable and our project leader had a bad experience with npm.

### Ionic with Cordova

As we wanted to have a cross-plattform application, we use Ionic (more on this later) with the integrated build system of the Cordova project to generate the application packages for (possibly) multiple plattforms.

### EsLint

To increase the code quality, we use EsLint to detect certain problems. Therefore, EsLint runs in our GitLab/CI environment. We recommend running it localy or using the VS Code plugin.

## Development

The app is basically a web application, which runs in a webview.

### TypeScript

As we wanted a cross-plattform application, we settled on TypeScript since the transpiler catches a lot of bugs during compile-time and not in production.

### Domain-Driven Design

Look at [DDD](https://en.wikipedia.org/wiki/Domain-driven_design#Building_blocks).

### Angular

Angular is our main development framework, especially for front-end development. To get started, there is a [tutorial](https://angular.io/tutorial).

### Ionic

The Ionic toolkit combines Angular, Cordova and the other stuff to build cross-plattform apps. Furthermore, it provides some mobile-specific UI elements.

### PouchDB

It is a lightweight, JavaScript-based NoSQL database. It stores data in the JSON format. It could later be synced with a central CouchDB.

### NgRx/Store

For state-management, we use NgRx Store, which is a heavily inspired version of Redux for Angular.
For more information, please visit [this site](https://ngrx.io/guide/store).

### Test Framworks (Jest und spectator)
